FROM registry.gitlab.com/lappis-unb/internal/devops/chatops/hubot-chatops-boilerplate

COPY external-scripts.json ./
COPY package.json ./
COPY scripts/gitlab.js ./scripts/

RUN apk --update add --no-cache --virtual build-dependencies    \
    git make gcc g++ python                                  && \
    npm install                                              && \
    apk del build-dependencies

ENTRYPOINT bin/hubot -a rocketchat
